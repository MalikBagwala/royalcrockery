from django.core.management import CommandError, BaseCommand
from ...models import SystemUser
import sys
import os


class Command(BaseCommand):
    help = "Crate a superuser, and allow password to be provided"

    def handle(self, *args, **options):
        password = os.environ.get("ADMIN_PASSWORD")
        if password:
            u, created = SystemUser.objects.update_or_create(
                username="admin",
                defaults={
                    "number": "9999999999",
                    "username": "admin",
                    "is_superuser": True,
                    "is_staff": True,
                },
            )
            u.set_password(password)
            u.save()
            if created:
                self.stdout.write(self.style.SUCCESS("Admin Created Successfully"))
            else:
                self.stdout.write(self.style.WARNING("Admin Updated Successfully"))
        else:
            CommandError(
                "Please input a password for superuser via an environment variables (ADMIN_PASSWORD)"
            )

        sys.exit()
