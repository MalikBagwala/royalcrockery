from django.core.management.base import BaseCommand, CommandError
from royalcrockery.ecommerce.models import Media


class Command(BaseCommand):
    help = 'Remove all the media objects from the s3 bucket which are not stored in db'

    def handle(self, *args, **options):
        media = Media.objects.all()
        
        message = f"Success! {len(media)} Photos"
        self.stdout.write(self.style.SUCCESS(message))
      # self.stdout.write(self.style.SUCCESS(
      #     'Successfully closed poll "%s"' % poll_id))
