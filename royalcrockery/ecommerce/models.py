from datetime import datetime
import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import JSONField
from django.core.validators import MinValueValidator, MaxValueValidator
from royalcrockery.ecommerce.helpers import functions
from django.utils.text import slugify


class Timestamp(models.Model):
    created_at = models.DateTimeField(verbose_name="Created At", auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="Updated At", auto_now=True)

    class Meta:
        abstract = True


class UID(models.Model):
    uuid = models.UUIDField(unique=True, editable=False, default=uuid.uuid4)

    class Meta:
        abstract = True


def get_image_path(instance, filename):
    ext = filename.split(".")[-1]
    code = instance.code
    return f"{code}.{ext.lower()}"


class Media(Timestamp):

    # Plural to prevent collision with MediaType (GraphQL Object)
    class MediaType(models.TextChoices):
        PHOTO = "PHT"
        VIDEO = "VID"
        DOCUMENT = "DOC"

    type = models.CharField(
        max_length=3, choices=MediaType.choices, default=MediaType.PHOTO
    )
    category = (
        models.CharField(max_length=10, default="product", null=True, blank=True),
    )
    code = models.CharField(
        max_length=22, editable=False, unique=True, default=functions.url_code
    )
    title = models.CharField(max_length=255, null=True, blank=True)
    name = models.FileField(upload_to=get_image_path)

    class Meta:
        verbose_name_plural = "Media"

    def __str__(self):
        return self.title


class SystemUser(AbstractUser, Timestamp):
    number = models.CharField(
        max_length=10, verbose_name="Phone Number", unique=True, null=True
    )
    uuid = models.UUIDField(unique=True, editable=False, default=uuid.uuid4)
    date_of_birth = models.DateField(
        verbose_name="Date Of Birth", null=True, blank=True
    )
    profile_photo = models.ForeignKey(
        Media, on_delete=models.CASCADE, null=True, blank=True
    )
    is_verified = models.BooleanField(default=False)

    @property
    def age(self):
        if self.date_of_birth is not None:
            return int((datetime.now().date() - self.date_of_birth).days / 365.25)
        return None

    @property
    def full_name(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        return self.first_name + " " + self.last_name

    pass


class Company(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Companies"

    pass


class Brand(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    pass


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"

    pass


class Tax(models.Model):
    name = models.CharField(max_length=60)
    codename = models.CharField(max_length=20, unique=True)
    percentage = models.DecimalField(max_digits=4, decimal_places=2, default=0)

    def __str__(self):
        return f"{self.codename}-{self.percentage}%"

    class Meta:
        verbose_name_plural = "Taxes"


class Coupon(models.Model):
    name = models.CharField(max_length=60)
    codename = models.CharField(max_length=20, unique=True)
    valid_from = models.DateTimeField()
    valid_till = models.DateTimeField()
    percentage = models.DecimalField(max_digits=4, decimal_places=2, default=0)

    def __str__(self):
        return self.name


class Product(Timestamp):
    PRODUCT_STATUSES = (
        ("IS", "In Stock"),
        ("OS", "Out Of Stock"),
        ("CS", "Coming Soon"),
        ("DC", "Discontinued"),
    )
    status = models.CharField(max_length=2, choices=PRODUCT_STATUSES, default="IS")
    uuid = models.UUIDField(unique=True, editable=False, default=uuid.uuid4)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    short_description = models.CharField(max_length=255)
    long_description = models.CharField(max_length=255)
    properties = JSONField(null=True, blank=True)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    discounted_price = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    categories = models.ManyToManyField(Category)
    photos = models.ManyToManyField(Media)
    stock = models.PositiveIntegerField(default=0)

    @property
    def slug(self):
        return slugify(self.name)

    def __str__(self):
        return self.name

    pass


class Rating(Timestamp):
    user = models.ForeignKey(SystemUser, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    rating = models.IntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(5)]
    )
    review = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.product} : {self.rating}"

    class Meta:
        unique_together = ("user", "product")


class Address(models.Model):
    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255)
    line3 = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    pin_code = models.CharField(max_length=12)
    country = models.CharField(max_length=255)
    longitude = models.FloatField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Addresses"

    def __str__(self):
        return self.line1


class Order(Timestamp, UID):
    ORDER_STATUES = (
        # Initlized the order (equivalent to shopping cart)
        ("CRT", "Created"),
        # Cancelled by the user after checking out or payment
        ("CBU", "Cancelled By User"),
        ("CBV", "Cancelled By Vendor"),
        # The payment amount successfully refunded
        ("RFD", "Refunded"),
        # This indicates that order is still processing (either payment or shipment)
        ("PND", "Pending"),
        # This indicates that the order is completed (payment done and shipped)
        ("CMP", "Completed"),
    )
    code = models.CharField(
        max_length=10, editable=False, unique=True, default=functions.code
    )
    razorpay_order_id = models.CharField(
        max_length=40, null=True, blank=True, unique=True
    )
    coupon = models.ForeignKey(Coupon, on_delete=models.PROTECT, blank=True, null=True)
    taxes = models.ManyToManyField(Tax)
    amount = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    # total = models.FloatField(default=0)
    # code = fields.CodeField()
    user = models.ForeignKey(
        SystemUser, on_delete=models.PROTECT, null=True, blank=True
    )
    status = models.CharField(max_length=3, choices=ORDER_STATUES, default="CRT")
    rating = models.IntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(5)], blank=True, null=True
    )
    comments = models.TextField(null=True, blank=True)
    billing_address = models.ForeignKey(
        Address, on_delete=models.PROTECT, null=True, blank=True
    )
    shipping_address = models.ForeignKey(
        Address,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        related_name="order_shipping_address",
    )

    @property
    def total(self):
        order_items = OrderItem.objects.filter(order=self.id)
        total_amount = 0
        for item in order_items:
            total_amount += item.paid_price * item.quantity
            pass
        return total_amount

    @property
    def payment_id(self):
        payments = Payment.objects.filter(order=self.id)
        for payment in payments:
            if payment.razorpay_payment_id is not None:
                return payment.razorpay_payment_id
        return None

    def __str__(self):
        return f"{self.user}-{self.code}"

    pass


class Payment(Timestamp, UID):
    class PaymentStatusType(models.TextChoices):
        SUCCESSFUL = "SUCC"
        FAILED = "FAIL"
        PROCESSING = "PROC"
        CANCELLED = "CANC"

    # Cant delete a user once he makes a transanction
    user = models.ForeignKey(SystemUser, on_delete=models.PROTECT)
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    currency = models.CharField(max_length=3, default="INR")

    razorpay_payment_id = models.CharField(
        max_length=40, unique=True, null=True, blank=True
    )
    razorpay_payment_signature = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(
        max_length=4,
        choices=PaymentStatusType.choices,
        default=PaymentStatusType.PROCESSING,
    )

    def __str__(self):
        return f"{self.razorpay_payment_id} - ₹{self.amount}"


class OrderStatus(Timestamp):
    ORDER_STATUES = (
        # Initlized the order (equivalent to shopping cart)
        ("CRT", "Created"),
        # Cancelled by the user after checking out or payment
        ("CBU", "Cancelled By User"),
        ("CBV", "Cancelled By Vendor"),
        # The payment amount successfully refunded
        ("RFD", "Refunded"),
        # This indicates that order is still processing (either payment or shipment)
        ("PND", "Pending"),
        # This indicates that the order is completed (payment done and shipped)
        ("CMP", "Completed"),
    )
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    status = models.CharField(max_length=3, choices=ORDER_STATUES, default="CRT")
    comment = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Order Statuses"


class OrderItem(Timestamp):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(default=0)
    price = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    paid_price = models.DecimalField(max_digits=8, decimal_places=2, default=0)

    class Meta:
        unique_together = ("order", "product")
