import graphene
from royalcrockery.ecommerce.models import Address
from graphene_django.types import DjangoObjectType


class AddressType(DjangoObjectType):
    class Meta:
        model = Address


class AddressInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    line1 = graphene.String(required=True)
    line2 = graphene.String(required=False)
    line3 = graphene.String(required=False)
    city = graphene.String(required=False)
    state = graphene.String(required=False)
    pin_code = graphene.Int(required=False)
    country = graphene.String(required=False)
    longitude = graphene.Float(required=True)
    latitude = graphene.Float(required=True)


class AddUpdateAddress(graphene.Mutation):
    address = graphene.Field(AddressType)

    class Arguments:
        address = graphene.Argument(AddressInput)

    def mutate(self, info, **arg):
        address = arg.get("address")
        addr, created = Address.objects.update_or_create(
            id=address.id,
            defaults=address
        )

        return AddUpdateAddress(address=addr)


class AddressMutation:
    add_update_address = AddUpdateAddress.Field()
    pass


class Query:
    addresses = graphene.List(AddressType)
    address = graphene.Field(AddressType, id=graphene.ID())

    def resolve_addresses(self, info, **kwargs):
        return Address.objects.all()

    def resolve_address(self, info, **kwargs):
        return Address.objects.get(pk=kwargs.get("id"))
