import graphene
from royalcrockery.ecommerce.models import Order, OrderItem, Tax, Product, Payment, SystemUser, Address
from .user import UserInput
from .address import AddressInput
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required
import razorpay
import os
from django.core.serializers.json import DjangoJSONEncoder
client = razorpay.Client(
    auth=(os.getenv("RAZORPAY_KEY_ID"), os.getenv("RAZORPAY_SECRET_KEY")))

# Genre Queries


class APIException(Exception):

    def __init__(self, message, status=None):
        self.context = {}
        if status:
            self.context['status'] = status
        super().__init__(message)


class TaxType(DjangoObjectType):
    class Meta:
        model = Tax


class OrderItemType(DjangoObjectType):
    class Meta:
        model = OrderItem


class PaymentType(DjangoObjectType):

    class Meta:
        model = Payment


class OrderItemInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    product = graphene.ID(required=True)
    quantity = graphene.Int(required=True)


class OrderType(DjangoObjectType):
    total = graphene.Float()
    taxes = graphene.List(TaxType)
    items = graphene.List(OrderItemType)
    payments = graphene.List(PaymentType)
    payment_id = graphene.String()
    @graphene.resolve_only_args
    def resolve_taxes(self):
        return self.taxes.all()

    @graphene.resolve_only_args
    def resolve_items(self):
        return OrderItem.objects.filter(order=self.id)

    @graphene.resolve_only_args
    def resolve_payments(self):
        return Payment.objects.filter(order=self.id)

    class Meta:
        model = Order


class OrderQuery(object):
    orders = graphene.List(OrderType)
    order = graphene.Field(OrderType, id=graphene.ID())
    my_orders = graphene.List(OrderType)

    @login_required
    def resolve_my_orders(self, info, **kwargs):
        user = info.context.user
        return Order.objects.filter(user=user.id)

    def resolve_orders(self, info, **kwargs):
        return Order.objects.all()

    def resolve_order(self, info, **kwargs):
        return Order.objects.get(pk=kwargs.get("id"))


class AddUpdateOrder(graphene.Mutation):
    order = graphene.Field(OrderType)

    class Arguments:
        id = graphene.ID(required=False)
        items = graphene.List(OrderItemInput, required=True)
        shipping_address = graphene.ID(required=True)
        billing_address = graphene.ID(required=True)

    def mutate(self, info, **arg):
        id = arg.get("id", None)
        user = info.context.user.id
        items = arg.get("items")
        shipping_address = arg["shipping_address"]
        billing_address = arg["billing_address"]

        order, created = Order.objects.update_or_create(
            id=id,
            defaults={"user_id": user}
        )
        if(order.payment_id is not None):
            raise APIException(
                message="The order has already been checked out", status=400)

        order.amount = 0
        for item in items:
            product = Product.objects.get(pk=item.product)
            itm, created = OrderItem.objects.update_or_create(
                id=item.id,
                defaults={"order_id": order.id,
                          "product_id": product.id,
                          "price": product.price,
                          "paid_price": product.discounted_price,
                          "quantity": item.quantity
                          })
            if(itm is not None):
                order.amount += itm.quantity * itm.paid_price
            pass

        if(order.payment_id is not None):
            raise APIException(
                message="The order has already been checked out", status=400)

        razorOrder = client.order.create(data={
            "amount": int(order.total*100),
            "currency": "INR",
            "receipt": order.code,
            "payment_capture": 1,
        })

        order.razorpay_order_id = razorOrder.get("id")
        order.shipping_address_id = shipping_address
        order.billing_address_id = billing_address
        order.status = "PND"
        # Apply GST as the default tax
        order.taxes.set(Tax.objects.filter(codename="GST"))
        order.save()
        return AddUpdateOrder(order=order)


class CapturePayment(graphene.Mutation):
    payment = graphene.Field(PaymentType)

    class Arguments:
        razorpay_order_id = graphene.String()
        razorpay_payment_id = graphene.String()
        razorpay_payment_signature = graphene.String()

    def mutate(self, info, **arg):
        razorpay_payment_signature = arg.get("razorpay_payment_signature")
        razorpay_order_id = arg.get("razorpay_order_id")
        razorpay_payment_id = arg.get("razorpay_payment_id")
        order = Order.objects.get(
            razorpay_order_id=razorpay_order_id)
        payment = Payment.objects.create(
            user_id=order.user.id,
            order_id=order.id,
            amount=order.total,
            razorpay_payment_id=razorpay_payment_id,
            razorpay_payment_signature=razorpay_payment_signature,
            status="SUCC"
        )

        return CapturePayment(payment=payment)


class OrderMutation:
    add_update_order = AddUpdateOrder.Field()
    capture_payment = CapturePayment.Field()
    pass
