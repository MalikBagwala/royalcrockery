import graphene
from royalcrockery.ecommerce.models import Product
from graphene_django.types import DjangoObjectType
from django.db.models.query_utils import Q

# Genre Queries


class ProductType(DjangoObjectType):
    slug = graphene.String()

    class Meta:
        model = Product


class ProductQuery(object):
    products = graphene.List(ProductType, search=graphene.String())
    product = graphene.Field(ProductType, id=graphene.ID())

    def resolve_products(self, info, **kwargs):
        search = kwargs.get("search")

        if search:
            # When there are lots of rows to filter out, its probably a good idea to add an ID wise filter
            # Rather than including them in the full text search
            filter = (
                Q(name__icontains=search) |
                Q(short_description__icontains=search) |
                Q(long_description__icontains=search) |
                Q(brand__name__icontains=search) |
                Q(brand__company__name__icontains=search)
            )
            return Product.objects.filter(filter)

        return Product.objects.all()

    def resolve_product(self, info, **kwargs):
        return Product.objects.get(pk=kwargs.get("id"))
