import graphene
from royalcrockery.ecommerce.models import SystemUser, Address, Order
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required
from django.core.mail import send_mail
from ..helpers.twilioclient import send_mobile_verification, verify_mobile


class SystemUserType(DjangoObjectType):
    full_name = graphene.String()
    age = graphene.Int()

    class Meta:
        model = SystemUser


class UserInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    first_name = graphene.String()
    last_name = graphene.String()
    number = graphene.String(required=False)
    email = graphene.String()
    password = graphene.String()


class UserQuery(object):
    users = graphene.List(SystemUserType)
    me = graphene.Field(SystemUserType)
    # my_orders = graphene.List(OrderType)

    @login_required
    def resolve_me(self, info, **kwargs):
        user = info.context.user
        return SystemUser.objects.get(pk=user.id)

    @login_required
    def resolve_users(self, info, **kwargs):
        return SystemUser.objects.all()


class AddUpdateUser(graphene.Mutation):
    user = graphene.Field(SystemUserType)

    class Arguments:
        user = graphene.Argument(UserInput)

    def mutate(self, info, **arg):
        user = arg.get("user")
        usr, created = SystemUser.objects.update_or_create(
            id=user.id,
            defaults={
                "username": user.number,
                **user,
            })
        usr.set_password(user.password)
        usr.save()
        return AddUpdateUser(user=usr)


class ForgotPassword(graphene.Mutation):
    message = graphene.String()

    class Arguments:
        email = graphene.String()

    def mutate(self, info, **arg):
        email = arg.get("email")
        try:
            mail = send_mail(
                'Subject here',
                'Here is the message.',
                'm.bagwala@outlook.com',
                [email],
                fail_silently=False,
            )
        except ValueError:
            print(ValueError)
        print(mail)
        return ForgotPassword(message="Done")


class SendPhoneVerification(graphene.Mutation):
    message = graphene.String()
    status = graphene.String()
    date_created = graphene.DateTime()
    date_updated = graphene.DateTime()

    class Arguments:
        number = graphene.String(required=True)

    def mutate(self, info, **arg):
        number = arg.get("number")
        v = send_mobile_verification(number)
        if(v.sid is not None):
            return SendPhoneVerification(message=f"Opt sent successfully to the mobile number {number}", status="OK", date_created=v.date_created, date_updated=v.date_updated)
        else:
            return SendPhoneVerification(message="Unable to send the otp, please try again", status="FAIL", date_created=v.date_created, date_updated=v.date_updated)


class VerifyUser(graphene.Mutation):
    user = graphene.Field(SystemUserType)
    message = graphene.String()
    status = graphene.String()

    class Arguments:
        otp = graphene.String(required=True)
        number = graphene.String(required=True)

    def mutate(self, info, **arg):
        otp = arg.get("otp")
        number = arg.get("number")

        usr = SystemUser.objects.get(number=number)
        if(usr.is_verified):
            return VerifyUser(user=usr, message="The user is already verified", status="OK")
        else:
            v = verify_mobile(number, otp)
            if v.valid:
                usr.is_verified = True
                usr.save()
                return VerifyUser(user=usr, status="OK", message="Phone number verified successfully!")
            else:
                return VerifyUser(
                    user=None, status="FAIL", message="Unable to verify the phone number, please check the sent otp")


class Mutation(object):
    add_update_user = AddUpdateUser.Field()
    forgot_password = ForgotPassword.Field()
    send_phone_verification = SendPhoneVerification.Field()
    verify_user = VerifyUser.Field()
    pass
