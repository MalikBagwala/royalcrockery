import graphene
from graphene_file_upload.scalars import Upload
from ..models import Media
from graphene_django.types import DjangoObjectType


class FileType(DjangoObjectType):
    class Meta:
        model = Media

    @graphene.resolve_only_args
    def resolve_name(self):
        return self.name and self.name.url


class UploadPhoto(graphene.Mutation):
    class Arguments:
        files = graphene.List(Upload, required=True)

    photos = graphene.List(FileType)

    def mutate(self, info, files, **kwargs):
        # files = Array of MultiPart file objects
        # Maps each file binary to a django Media object (since it is tied with boto3 the image is uploaded automatically to S3)
        photos = Media.objects.bulk_create(map(lambda file: Media(name=file), files))
        return UploadPhoto(photos)


class Mutation(object):
    upload_photo = UploadPhoto.Field()
    pass


class Query(object):
    photos = graphene.List(FileType)

    def resolve_photos(self, info, **kwargs):
        return Media.objects.all()

    photo = graphene.Field(FileType, id=graphene.ID(required=True))

    def resolve_photo(self, info, id):
        return Media.objects.get(pk=id)
