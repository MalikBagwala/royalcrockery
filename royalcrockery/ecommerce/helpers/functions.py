import secrets
import random


def code(length=5):
    return secrets.token_hex(length).upper()


def otp():
    return random.randint(100000, 999999)


def url_code():
    return secrets.token_urlsafe(16)
