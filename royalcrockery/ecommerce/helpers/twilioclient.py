from twilio.rest import Client
from os import environ

verification_sid = environ.get("TWILIO_VERIFICATION_SID")
twilioclient = Client(environ.get("TWILIO_ACCOUNT_SID"),
                      environ.get("TWILIO_AUTH_TOKEN"))


def send_mobile_verification(number):
    return twilioclient.verify.services(verification_sid).verifications.create(to=f'+91{number}', channel='sms')


def verify_mobile(number, code):
    check = twilioclient.verify.services(
        verification_sid).verification_checks.create(to=f'+91{number}', code=code)
    return check
