import graphene

# from movies_graphql.moviedb.models import Genre
# from movies_graphql.moviedb.genre.genretypes import GenreType
from royalcrockery.ecommerce.models import Product, Brand, Company, Address
from graphene_django.types import DjangoObjectType
from .graphql import order, user, product, address, media
import graphql_jwt
# All Genre Types


class BrandType(DjangoObjectType):
    class Meta:
        model = Brand


class CompanyType(DjangoObjectType):
    class Meta:
        model = Company


class AdressType(DjangoObjectType):
    class Meta:
        model = Address


class Query(product.ProductQuery, order.OrderQuery, user.UserQuery, address.Query, media.Query, graphene.ObjectType):
    brands = graphene.List(BrandType)
    companies = graphene.List(CompanyType)

    def resolve_brands(self, info, **kwargs):
        return Product.objects.all()

    def resolve_companies(self, info, **kwargs):
        return Company.objects.all()

    pass


class Mutation(order.OrderMutation, address.AddressMutation, user.Mutation, media.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
