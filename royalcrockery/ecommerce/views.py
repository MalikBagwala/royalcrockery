from django.http import JsonResponse
import datetime
from secrets import token_hex


def current_datetime(request):
    now = datetime.datetime.now()
    return JsonResponse(
        {"time": now, "message": "This is the new message", "token": token_hex(16),}
    )
