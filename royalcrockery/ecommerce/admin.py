from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from royalcrockery.ecommerce import models
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import AbstractUser

from django.utils.html import format_html
import os
import boto3

client = boto3.client("s3")
# Clean Up The Headers
admin.site.site_title = ""
admin.site.index_title = ""


class UserForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = models.SystemUser


@admin.register(models.SystemUser)
class SystemUserAdmin(UserAdmin):
    list_display = UserAdmin.list_display + ("number", "uuid")
    fieldsets = UserAdmin.fieldsets + (
        (
            "Extra Fields",
            {
                "fields": [
                    f
                    for f in [field.name for field in models.SystemUser._meta.fields]
                    if (
                        f not in [field.name for field in AbstractUser._meta.fields]
                        and f not in ["created_at", "updated_at", "uuid"]
                        and f != models.SystemUser._meta.pk.name
                    )
                ],
            },
        ),
    )
    pass


def hard_delete(modeladmin, request, queryset):
    for photo in queryset:
        client.delete_object(Bucket="royalcrockery", Key=photo.name.name)
        photo.delete()
        pass
    pass


hard_delete.short_description = "Delete database rows as well as images in s3"


class MediaAadmin(admin.ModelAdmin):
    list_display = ("title", "name", "created_at")
    actions = [hard_delete]
    pass


admin.site.register(models.Brand)
admin.site.register(models.Company)
admin.site.register(models.Media, MediaAadmin)
admin.site.register(models.Product)
admin.site.register(models.Rating)
admin.site.register(models.Category)
admin.site.register(models.Order)
admin.site.register(models.OrderItem)
admin.site.register(models.Payment)
admin.site.register(models.OrderStatus)
admin.site.register(models.Tax)
admin.site.register(models.Coupon)
admin.site.register(models.Address)
