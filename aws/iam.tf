resource aws_iam_role ecs {
  name               = "${local.prefix}-ecs-role"
  assume_role_policy = file("./role-policy.json")
  tags               = local.tags
}

resource aws_iam_role_policy_attachment ecs-policy {
  role       = aws_iam_role.ecs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


# IAM Role for uploading images to the S3 Bucket
resource aws_iam_role s3 {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./role-policy.json")

  tags = local.tags
}

resource aws_iam_policy s3 {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow access to the django server to the S3 bucket"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:PutObject",
          "s3:GetObjectAcl",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:DeleteObject",
          "s3:PutObjectAcl"
        ],
        "Resource" : [
          "${aws_s3_bucket.s3.arn}/*",
          "${aws_s3_bucket.s3.arn}"
        ]
      }
    ]
    }
  )
}

resource aws_iam_role_policy_attachment s3 {
  role       = aws_iam_role.s3.name
  policy_arn = aws_iam_policy.s3.arn
}
