resource "aws_cloudfront_distribution" "main" {
  origin {
    domain_name = aws_s3_bucket.s3.bucket_regional_domain_name
    origin_id   = "S3-${aws_s3_bucket.s3.id}"
  }
  aliases = ["${var.subdomain.cdn[terraform.workspace]}.${var.domain}"]
  enabled = true
  viewer_certificate {
    minimum_protocol_version = "TLSv1.2_2018"
    acm_certificate_arn      = var.cloudfront_acm_arn
    # Do NOT choose ssl_support_method = vip (It can incur heavy extra charges 600$+ )
    ssl_support_method = "sni-only"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "allow-all"
    target_origin_id       = "S3-${aws_s3_bucket.s3.id}"
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }
}
