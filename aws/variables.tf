# Project Variables

variable region {
  type        = string
  default     = "ap-south-1" # Asia Pacific - Mumbai
  description = "The region to build all the infastructure in"
}

variable project {
  type        = string
  default     = "Royalcrockery"
  description = "Name of the project"
}

variable contact {
  type        = string
  default     = "m.bagwala@outlook.com"
  description = "Name of the maintainer / manager"
}

variable prefix {
  type        = string
  default     = "rk"
  description = "A simple and short prefix to apply to all the aws resources"
}

# DNS
variable domain {
  type        = string
  default     = "royalcrockery.com"
  description = "example.com"
}

variable subdomain {
  type        = map(map(string))
  description = "List Of subdomains"

  default = {
    api : {
      staging : "api-staging",
      production : "api"
    },
    cdn : {
      staging : "cdn-staging",
      production : "cdn"
    }
  }
}

# Container to add to Load Balancer
variable container_to_load_balancer {
  type        = string
  default     = "rk-nginx"
  description = "The name of the container to load balance (preferrably nginx)"
}


variable twilio_account_sid {
  type = string

}

variable twilio_auth_token {
  type = string
}


variable twilio_messaging_sid {
  type = string
}

variable twilio_verification_sid {
  type = string
}

variable admin_password {
  type = string
}


variable razorpay_key_id {
  type = string
}

variable razorpay_secret_key {
  type = string
}



# ECR Django Image
variable ecr_api_url {
  type        = string
  default     = "619797457674.dkr.ecr.ap-south-1.amazonaws.com/royalcrockery-api:latest"
  description = "The url to django image"
}

variable ecr_nginx_url {
  type        = string
  default     = "malikbagwala/django-nginx"
  description = "The url to nginx proxy"
}

# US-EAST SSL Certificate (FOR CloudFron Distribution)

variable cloudfront_acm_arn {
  type        = string
  default     = "arn:aws:acm:us-east-1:619797457674:certificate/1f4a77de-ad1f-475e-bc57-1c60d38d9338"
  description = "The ACM certificate used for cdn"
}
