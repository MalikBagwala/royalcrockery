postgres_user     = xx
postgres_password = xx

# Django Secret key
secret_key = xx

# Twilio
twilio_account_sid      = xx
twilio_auth_token       = xx
twilio_messaging_sid    = xx
twilio_verification_sid = xx

# Superuser Password
admin_password = xx

# Razorpay Credentials
razorpay_key_id     = xx
razorpay_secret_key = xx


# AWS Credentials
aws_access_key_id     = xx
aws_secret_access_key = xx
