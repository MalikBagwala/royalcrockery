/**
  Required Variables
*/
resource "random_password" "postgres" {
  length           = 16
  special          = true
  override_special = "_%@"

}

resource random_string user {
  length  = 10
  lower   = true
  number  = true
  special = false
}

# Security Group
# Allows access to RDS only from the ECS service and not publically

resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    protocol        = "tcp"
    from_port       = 5432
    to_port         = 5432
    security_groups = [aws_security_group.ecs_service.id]
  }

  tags = local.tags
}

# RDS Instance Definition

resource "aws_db_instance" "postgres" {
  identifier             = "${local.prefix}-postgres"
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "postgres"
  instance_class         = "db.t2.micro"
  name                   = lower(var.project)
  password               = random_password.postgres.result
  username               = random_string.user.result
  multi_az               = false
  vpc_security_group_ids = [aws_security_group.rds.id]
  tags                   = local.tags
  # Conditional Settings
  skip_final_snapshot     = terraform.workspace == "production" ? false : true
  backup_retention_period = terraform.workspace == "production" ? 7 : 0
  copy_tags_to_snapshot   = terraform.workspace == "production" ? true : false
}
