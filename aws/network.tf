
# Get the default VPC
data "aws_vpc" "default" {
  default = true
}

# Get the default SUBNETS
data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}
