resource aws_s3_bucket s3 {
  bucket = "${local.prefix}-media"
  acl    = "public-read"
  # Will empty the bucket before destroying
  force_destroy = true
  tags          = local.tags
}
