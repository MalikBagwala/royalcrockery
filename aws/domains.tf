

data "aws_route53_zone" "zone" {
  name = "${var.domain}."
}

resource "aws_route53_record" "api" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${var.subdomain.api[terraform.workspace]}.${data.aws_route53_zone.zone.name}"
  type    = "A"

  alias {
    name                   = aws_lb.api.dns_name
    zone_id                = aws_lb.api.zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "cdn" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${var.subdomain.cdn[terraform.workspace]}.${data.aws_route53_zone.zone.name}"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.main.domain_name
    zone_id                = aws_cloudfront_distribution.main.hosted_zone_id
    evaluate_target_health = false
  }
}
# Get The SSL certificate
data "aws_acm_certificate" "api" {
  domain      = "*.${var.domain}"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}
