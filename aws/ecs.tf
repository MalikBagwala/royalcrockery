// Log Group
resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"
  tags = local.tags
}

# Secret Key For Django (Generates a new one depending on the environment)
resource random_password django_secret_key {
  length  = 42
  special = true
  keepers = {
    "envrionment" = terraform.workspace
  }
}

resource "aws_ecs_task_definition" api {
  family = "${local.prefix}-api"
  container_definitions = jsonencode([
    {
      name              = "server",
      image             = var.ecr_api_url,
      essential         = true,
      memoryReservation = 256,
      environment : [
        {
          name  = "POSTGRES_HOST",
          value = aws_db_instance.postgres.address
        },
        {
          name  = "POSTGRES_USER",
          value = aws_db_instance.postgres.username
        },
        {
          name  = "POSTGRES_DB",
          value = aws_db_instance.postgres.name
        },
        {
          name  = "POSTGRES_PASSWORD",
          value = aws_db_instance.postgres.password
        },
        {
          name  = "TWILIO_ACCOUNT_SID",
          value = var.twilio_account_sid
        },
        {
          name  = "TWILIO_AUTH_TOKEN",
          value = var.twilio_auth_token
        },
        {
          name  = "TWILIO_MESSAGING_SID",
          value = var.twilio_messaging_sid
        },
        {
          name  = "TWILIO_VERIFICATION_SID",
          value = var.twilio_verification_sid
        },

        {
          name  = "AWS_STORAGE_BUCKET_NAME",
          value = aws_s3_bucket.s3.id
        },
        {
          name  = "RAZORPAY_KEY_ID",
          value = var.razorpay_key_id
        },
        {
          name  = "RAZORPAY_SECRET_KEY",
          value = var.razorpay_secret_key
        },
        {
          name  = "ADMIN_PASSWORD",
          value = var.admin_password
        },
        {
          name  = "SECRET_KEY",
          value = random_password.django_secret_key.result
        },
        {
          name  = "ALLOWED_HOSTS",
          value = aws_route53_record.api.fqdn
        },
        {
          name  = "ENV",
          value = terraform.workspace
        },
        {
          name  = "CDN_DOMAIN",
          value = aws_route53_record.cdn.fqdn
        }
      ],
      portMappings = [
        {
          containerPort = 9000,
          hostPort      = 9000
        }
      ],
      mountPoints = [
        {
          sourceVolume  = "static",
          containerPath = "/usr/src/app/static",
          readOnly      = false
        }
      ],
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-region : "ap-south-1",
          awslogs-group : aws_cloudwatch_log_group.ecs_task_logs.name,
          awslogs-stream-prefix : "django"
        }
      }
    },
    {
      name      = var.container_to_load_balancer,
      image     = var.ecr_nginx_url,
      essential = true,
      environment = [
        {
          name  = "SERVER_NAME",
          value = aws_route53_record.api.fqdn
        },
      ],
      portMappings = [
        {
          containerPort = 8000,
          hostPort      = 8000
        }
      ],
      memoryReservation = 256,
      mountPoints = [
        {
          sourceVolume  = "static",
          containerPath = "/usr/src/app/static",
          readOnly      = false
        }
      ],
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-region : "ap-south-1",
          awslogs-group : aws_cloudwatch_log_group.ecs_task_logs.name,
          awslogs-stream-prefix : "nginx"
        }
      }
    }
    ]
  )
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.ecs.arn
  task_role_arn            = aws_iam_role.s3.arn
  volume {
    name = "static"
  }
}

# Create a CLUSTER
resource aws_ecs_cluster api {
  name               = "${local.prefix}-cluster"
  capacity_providers = ["FARGATE_SPOT", "FARGATE"]
  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = 1
  }
  tags = local.tags
}

# # Create a SERVICE
resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.api.name
  task_definition = aws_ecs_task_definition.api.arn
  desired_count   = 1
  capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = 1
  }
  network_configuration {
    subnets          = data.aws_subnet_ids.default.ids
    security_groups  = [aws_security_group.ecs_service.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = var.container_to_load_balancer
    container_port   = 8000
  }

  depends_on = [aws_lb_listener.api_https]
}

# --- SECURITY GROUP ---
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = data.aws_vpc.default.id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 8000
    to_port         = 8000
    protocol        = "tcp"
    security_groups = [aws_security_group.lb.id]
  }

  tags = local.tags
}
