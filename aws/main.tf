terraform {
  backend "s3" {
    bucket         = "dev-malik-terraform-state"
    key            = "royalcrockery.tfstate"
    region         = "ap-south-1"
    encrypt        = true
    dynamodb_table = "terraform-lock"
  }
}

provider "aws" {
  region  = "ap-south-1"
  version = "~> 2.0"
}
provider "random" {
  version = "~> 2.2"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
