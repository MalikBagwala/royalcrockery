# The final API endpoint url
output api_url {
  value       = "https://${aws_route53_record.api.fqdn}"
  description = "Api endpoint url"
}

# Output RDS Host on the terminal
output rds_host {
  value = aws_db_instance.postgres.address
}


output cdn {
  value = "https://${aws_route53_record.cdn.fqdn}"
}
