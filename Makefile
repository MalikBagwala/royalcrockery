freeze:
	pip freeze | grep -v "pkg-resources" > requirements.txt
dev:
	docker-compose -f dev-compose.yml up -d
prod:
	docker-compose up --build --remove-orphans 
# Required AWS CLI 2
ecr-login:
	aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 619797457674.dkr.ecr.ap-south-1.amazonaws.com