FROM python:3.8.6-slim-buster
LABEL maintainer="m.bagwala@outlook.com"
# Environment Variables
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0
# The current working directory for the Django Application
WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apt-get update && apt-get install libpq-dev gcc -y
RUN pip install -r requirements.txt

COPY . .
RUN chmod +x ./server.sh

EXPOSE 8000

CMD [ "./server.sh" ]