# Royalcrockery

Royalcrockery is a backend api built on Django, Postgres, Docker, Terraform, AWS and GraphQL.

### Features!

- Full CRUD functionallity in the app
- Login system with OTP verification using twilio
- Creation of orders in the api and then integrate with the Razorpay API for real world transactions
- Multiple photo upload using GraphQL and then storing it on the `S3 bucket`
- Dockerized end-to-end pipeline and then have it deployed via `Terraform` using `Gitlab CI`
- The project uses `AWS ECS` to deploy django as a container (very scallable and production ready since you can run `n` number of containers with a click of a button)
- The images and media (not staticfiles) are served via CDN for better reach
- There is a custom domain associated with different environments
- Set up an `LB (Application Load Balancer)` to distribute Load between different tasks running in an ECS service (also, this is what allows us to set up a custom domain easily, since with each rendition of a task, the publically available IP address changes because it is ephimeral)
- Customized django admin interface with a much `nicer` looking ui
- A managed database service in the cloud (Amazon RDS) with appropriate security settings
- Django custom commands ( `createadmin` )
- Set up `Nginx` as a reverse proxy and static files server
- The project also utlizes a custom image (`malikbagwala/django-nginx`) hosted on `dockerhub`. The reason being the nginx image is very generic and it can used across multiple projects. Also keeping it in the same project causes unnecessary rebuilds (the nginx image may be re-built even when, only the django code changes)
- A proper `docker-compose` file to spin up a production environment, locally
- Uses `environment variables` to secure the secret values

### Tech

Dillinger uses a number of open source projects to work properly:

- [Terraform] - Infastructure as code!
- [GitLab] - A git distributor which includes CI/CD
- [AWS] - Cloud Provider
- [Docker] - The populer containerization tool
- [GraphQL] - REST in peace! :D
- [Python] - A fast interpreted language
- [Django] - The amazing backend framework for python

### Installation

1. Install Python (3.8 recommended) and set up Docker (with compose)
2. Replace `.env.example` with `.env` including appropriate values

```sh
docker-compose up
```

[django]: https://www.djangoproject.com/
[python]: https://python.org
[graphql]: https://graphql.org
[docker]: https://docker.io
[gitlab]: https://gitlab.io
[terraform]: https://www.terraform.io
[aws]: https://aws.amazon.com
